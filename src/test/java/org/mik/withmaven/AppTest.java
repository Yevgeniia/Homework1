package org.mik.withmaven;


/**
 * @author Yevgeniia Grebenuk EFD4C9
 *
 */

import org.junit.runner.RunWith;
import org.junit.runners.Suite;




@RunWith(Suite.class)
@Suite.SuiteClasses ({
	DomainTest.class
})

public class AppTest{
	
	
}

