package org.mik.withmaven;

/**
 * @author Yevgeniia Grebenuk EFD4C9
 *
 */
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.mik.withmaven.domain.AbstractPerson;
import org.mik.withmaven.domain.Person;
import org.mik.withmaven.domain.Specialization;
import org.mik.withmaven.domain.Student;

public class DomainTest {

	public final static String TEST_NAME = "Zaphod";
	public final static int TEST_YEAR = 2000;

	@Test
	public void testPerson() {
		assertTrue(Person.class.isInterface());
		assertTrue(Modifier.isAbstract(AbstractPerson.class.getModifiers()));
		try {
			assertNotNull(AbstractPerson.class.getMethod("getName"));
			assertNotNull(AbstractPerson.class.getMethod("setName", String.class));
			assertNotNull(AbstractPerson.class.getMethod("setBirthYear", int.class));
			Method m = AbstractPerson.class.getMethod("getBirthYear");
			assertNotNull(m);
			assertEquals(m.getReturnType(), int.class);

			Constructor<?> c = AbstractPerson.class.getConstructor(String.class, int.class);
			assertNotNull(c);

		} catch (Exception e) {
			fail(e.getMessage());
		}

		Student st = new Student(TEST_NAME, TEST_YEAR, Specialization.INFORMATICS);

		assertEquals(st.getName(), TEST_NAME);
		assertEquals(st.getBirthYear(), TEST_YEAR);
		st.setName("");
		assertEquals(st.getName(), TEST_NAME);
		assertEquals(st.getSpecialization(), Specialization.INFORMATICS);

		Student st1 = new Student(TEST_NAME, TEST_YEAR, Specialization.INFORMATICS);
		assertFalse(st == st1);
		assertEquals(st, st1);

	}

}
