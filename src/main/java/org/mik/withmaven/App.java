package org.mik.withmaven;

import org.mik.withmaven.domain.Institue;
import org.mik.withmaven.domain.Specialization;
import org.mik.withmaven.domain.Student;
import org.mik.withmaven.domain.Teacher;

/**
 * @author Yevgeniia Grebenuk EFD4C9
 *
 */
public class App 
{
    public static void main( String[] args )
    
    {
    	/**
    	 * Creating instances (Yevgeniia , Zamek ) from the  class 
    	 * and printing values with System.out.println();
    	 * 
    	 */
    	 Student Yevgeniia = new Student("Yevgeniia ", 1999, Specialization.INFORMATICS); //$NON-NLS-1$
         
                
         Teacher Zamek = new Teacher("Zamek Zoltan", 1990, Institue.AUTOMATION); //$NON-NLS-1$
         
         
         System.out.println(Yevgeniia);

         
         System.out.println(Zamek);
      
    }
}
