/**
 * 
 */
package org.mik.withmaven.domain;

/**
 * @author Yevgeniia Grebenuk EFD4C9
 *
 */
public abstract class AbstractPerson implements Person {

	public final static int MIN_NAME_LENGTH = 2;
	public final static int MIN_YEAR = 1950;
	public final static int MAX_YEAR = 2018;
	public final static String EMPTY = "";
	
	/**
	 * adding 2 private members ( name , birthYear )
	 */
	
	

	private String name;

	private int birthYear;
	
	public AbstractPerson() {

	
	}
	
	/**
	 * Generating constructor using Fields 
	 * 
	 */

	public AbstractPerson(String name, int birthYear) {
	
		this.name = name;
		this.birthYear = birthYear;
	}

/** 
 * Generating code :  getter and setter 
 * */
	public String getName() {
		return this.name == null ? EMPTY : this.name;
	}

	public void setName(String name) {
		if (name != null && name.length() >= MIN_NAME_LENGTH)
			this.name = name;

	}

	public int getBirthYear() {
		return this.birthYear;
		
	}

	public void setBirthYear(int year) {

		if (year < MIN_YEAR || year > MAX_YEAR)
			this.birthYear = year;

	}

	public boolean isStudent() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mik.withmaven.domain.Person#isTeacher()
	 */
	public boolean isTeacher() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mik.withmaven.domain.Person#isWorder()
	 */
	public boolean isWorker() {
		return false;
	}
/** 
 * Generating code : hash code and equals 
 * */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + birthYear;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractPerson other = (AbstractPerson) obj;
		if (birthYear != other.birthYear)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	/** 
	 * Generating code : toString
	 * 
	 * */
	 @Override
	public String toString() {
		return this.name + " (" + this.birthYear + ")" ;
	}

}
