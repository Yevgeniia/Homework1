package org.mik.withmaven.domain;
/**
 * @author Yevgeniia Grebenuk EFD4C9
 *
 */

public enum Institue {
	
	/**
	 * 
	 * Institue that can be used 
	 */
	
	INFORMATICS,MECHANICS , AUTOMATION

}
