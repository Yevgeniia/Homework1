package org.mik.withmaven.domain;
/**
 * @author Yevgeniia Grebenuk EFD4C9
 *
 */

public class Student extends AbstractPerson {
	

	/**
	 * adding 1 private member ( specialization )
	 */
	
	private Specialization specialization;
	
	/**
	 * Generating constructor using Fields 
	 * 
	 */

	public Student(String name, int year, Specialization specialization) {
		super(name,year);
		this.specialization = specialization;
	}
	
	public boolean isStudent() {
		return true;
		
	}
	
	/** 
	 * Generating code :  getter and setter 
	 * */

	public Specialization getSpecialization() {
		return this.specialization;
	}

	public void setSpecialization(Specialization specialization) {
		this.specialization = specialization;
	}
	
	/** 
	 * Generating code : hash code and equals 
	 * */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((specialization == null) ? 0 : specialization.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (specialization != other.specialization)
			return false;
		return true;
	}
	
	/** 
	 * Generating code : toString
	 * 
	 * */
	
	@Override
	public String toString() {
	
		return "Student: " + super.toString();
	}

}
