package org.mik.withmaven.domain;

/**
 * @author Yevgeniia Grebenuk EFD4C9
 *
 */
public interface Person {
	
	/**
	 * 
	 *method descriptions for getName, 
	 *setName,
	 *getBirthYear,
	 *setBirthYear,
	 *isStudent, 
	 *isTeacher,
	 *
	 *isWorker
	 */

	String getName();

	void setName(String name);

	int getBirthYear();

	void setBirthYear(int year);

	boolean isStudent();

	boolean isTeacher();

	boolean isWorker();
	
	

}
