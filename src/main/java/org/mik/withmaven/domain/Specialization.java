package org.mik.withmaven.domain;

/**
 * @author Yevgeniia Grebenuk EFD4C9
 *
 */

public enum Specialization {
	
	/** *
	 * 
	 * Specializations that can be used 
	 */
	
	INFORMATICS, ELECTRIC , MECHANICS, ARCHITECT 

}
