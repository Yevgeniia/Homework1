package org.mik.withmaven.domain;

/**
 * @author Yevgeniia Grebenuk EFD4C9
 *
 */
public class Teacher extends AbstractPerson {
	
	/**
	 * adding 1 private member ( specialization )
	 */
	
	
	private Institue institute;
	
	

	/**
	 * Generating constructor using Fields 
	 * 
	 */
	public Teacher(String name, int birthYear, Institue inst) {
		super(name, birthYear);
		this.institute = inst;
		// TODO Auto-generated constructor stub
	}
	
	/** 
	 * Generating code :  getter and setter 
	 * */

	public Institue getInstitute() {
		return institute;
	}

	public void setInstitute(Institue institute) {
		this.institute = institute;
	}
	
	/** 
	 * Generating code : hash code and equals 
	 * */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((institute == null) ? 0 : institute.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Teacher other = (Teacher) obj;
		if (institute != other.institute)
			return false;
		return true;
	}
	
	/** 
	 * Generating code : toString
	 * 
	 * */
	
	@Override
	public String toString() {
		
		return "Teacher: " + super.toString();
	}

}
